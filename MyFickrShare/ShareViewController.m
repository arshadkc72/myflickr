//
//  ShareViewController.m
//  MyFickrShare
//
//  Created by Arshad on 10/13/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import "ShareViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "SharedSuite.h"

#define FLICKR_KEY      @"bccee49853bfbb2af7c0d5581220f298"
#define FLICKR_SECRET   @"315c8502105c6516"

@interface ShareViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;
@property (strong , nonatomic) NSData* imageData;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (strong, nonatomic) OFFlickrAPIRequest *request;
@property (strong, nonatomic) OFFlickrAPIContext *context;
@end

@implementation ShareViewController

- (void) loadView
{
    [super loadView];
    NSExtensionContext *myExtensionContext = self.extensionContext;
    NSArray *inputItems = myExtensionContext.inputItems;
    NSExtensionItem* item = [inputItems objectAtIndex:0];
    NSLog(@"%@",inputItems);
    
    __weak ShareViewController* weakSelf = self;
    NSItemProvider *itemProvider = item.attachments.firstObject;
    if ([itemProvider hasItemConformingToTypeIdentifier:(NSString *)kUTTypeImage]) {
        __weak ShareViewController* weakSelfRef = weakSelf;
        [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage options:nil completionHandler:^(NSData* data, NSError *error) {
            UIImage* image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelfRef.selectedImage.image = image;
                weakSelfRef.imageData = data;
            });
        }];
    }
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[OFFlickrAPIContext alloc] initWithAPIKey:FLICKR_KEY sharedSecret:FLICKR_SECRET];
    self.request = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.context];
    self.request.delegate = self;
    
    NSString* oathToken = [[SharedSuite sharedInstance] oathToken];
    NSString* oathSecret = [[SharedSuite sharedInstance] oathSecret];
    
    BOOL loggedIn = [[SharedSuite sharedInstance] loggedIn];
    if(loggedIn){
        printf("\n User is already logged in");
        self.request.context.OAuthToken = oathToken;
        self.request.context.OAuthTokenSecret = oathSecret;
    }
    else{
        [self.shareButton setEnabled:NO];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary
{
    NSLog(@"%@",inResponseDictionary);
    [self.statusLabel setText:@"Done"];
    [self onDismiss:nil];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError
{
    NSLog(@"%@",[inError localizedDescription]);
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:inError.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest imageUploadSentBytes:(NSUInteger)inSentBytes totalBytes:(NSUInteger)inTotalBytes
{
    NSLog(@"uploaded %tu of %tu bytes",inSentBytes,inTotalBytes);

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.statusLabel setText:[NSString stringWithFormat:@"uploaded %tu of %tu bytes",inSentBytes,inTotalBytes]];
        if(inSentBytes == inTotalBytes)
            [self.statusLabel setText:@"Wait.."];
    });
}

#pragma mark Actions

- (IBAction)onShare:(id)sender {
    
    if([[SharedSuite sharedInstance] loggedIn] == NO){
        __weak ShareViewController* weakSelf = self;
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please login in MyFLickr" preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            [weakSelf onDismiss:nil];
        }];
        return;
    }
    if(self.imageData){
        NSInputStream *imageStream = [NSInputStream inputStreamWithData:self.imageData];
        [self.request uploadImageStream:imageStream suggestedFilename:@"Foobar.jpg" MIMEType:@"image/jpeg" arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"0", @"is_public", nil]];
        [self.statusLabel setText:@"Uploading.."];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Oops" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];

        [self presentViewController:alert animated:YES completion:nil];
    }
}


- (IBAction)onDismiss:(id)sender {
    [UIView animateWithDuration:0.20 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
    }];
}

@end
