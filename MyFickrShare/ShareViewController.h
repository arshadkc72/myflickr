//
//  ShareViewController.h
//  MyFickrShare
//
//  Created by Arshad on 10/13/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ObjectiveFlickr.h>


@interface ShareViewController : UIViewController<NSExtensionRequestHandling,OFFlickrAPIRequestDelegate>

@end
