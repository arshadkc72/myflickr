MyFlickr

A very basic App to demonstrate Share extension in iOS 8.

1. MyFlickr helps you to browse through your recent flickr photos.
2. Use MyFlickr Share extension from Photo Library to upload images to your flickr account.
3. When you launch app for first time, you ll be asked to login to flickr account.