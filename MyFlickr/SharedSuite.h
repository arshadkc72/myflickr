//
//  SharedSuite.h
//  MyFlickr
//
//  Created by Arshad on 10/15/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedSuite : NSObject
+ (id)sharedInstance;
- (void)save;

@property (strong,nonatomic) NSString* oathToken;
@property (strong,nonatomic) NSString* oathSecret;
@property (nonatomic) BOOL loggedIn;
@end
