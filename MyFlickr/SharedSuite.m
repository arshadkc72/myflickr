//
//  SharedSuite.m
//  MyFlickr
//
//  Created by Arshad on 10/15/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import "SharedSuite.h"

@interface SharedSuite ()
@property(nonatomic,strong) NSUserDefaults* userDefaults;
@end

@implementation SharedSuite
@synthesize oathToken = _oathToken, oathSecret = _oathSecret;

+ (instancetype) sharedInstance
{
    static dispatch_once_t onceToken;
    static SharedSuite* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[SharedSuite alloc] initWithSharedSuit];
    });
    return instance;
}

- (id)initWithSharedSuit
{
    self = [super init];
    if(self){
        self.userDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.syncplicity.ios"];
    }
    return self;
}

- (void)save
{
    [self.userDefaults synchronize];
}

- (NSString*) oathToken
{
    return [self.userDefaults objectForKey:@"oathToken"];
}

- (void) setOathToken:(NSString *)oathToken
{
    [self.userDefaults setObject:oathToken forKey:@"oathToken"];
}

- (NSString*) oathSecret
{
    return [self.userDefaults objectForKey:@"oathSecret"];
}

- (void) setOathSecret:(NSString *)oathSecret
{
    [self.userDefaults setObject:oathSecret forKey:@"oathSecret"];
}

- (BOOL) loggedIn
{
    return [[self.userDefaults objectForKey:@"loggedIn"] boolValue];
}

- (void) setLoggedIn:(BOOL)loggedIn
{
    [self.userDefaults setObject:[NSNumber numberWithBool:loggedIn] forKey:@"loggedIn"];
}

@end
