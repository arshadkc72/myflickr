//
//  ViewController.m
//  MyFlickr
//
//  Created by Arshad on 10/13/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import "ViewController.h"
#import "ImageCollectionViewCell.h"
#import "SharedSuite.h"
#import "Defines.h"

#define FLICKR_KEY      @"bccee49853bfbb2af7c0d5581220f298"
#define FLICKR_SECRET   @"315c8502105c6516"

@interface ViewController ()
{
    NSMutableArray* photos;
}
@property (strong, nonatomic) OFFlickrAPIRequest *request;
@property (strong, nonatomic) OFFlickrAPIContext *context;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.refreshButton setEnabled:NO];
    
    self.context = [[OFFlickrAPIContext alloc] initWithAPIKey:FLICKR_KEY sharedSecret:FLICKR_SECRET];
    self.request = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.context];
    
    photos = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleURL:) name:handleURL object:nil];
    self.request.delegate = self;

    BOOL loggedIn = [[SharedSuite sharedInstance] loggedIn];
    if(loggedIn){
        printf("\n User is already logged in");
        [self.refreshButton setEnabled:YES];
        NSString* oathToken = [[SharedSuite sharedInstance] oathToken];
        NSString* oathSecret = [[SharedSuite sharedInstance] oathSecret];
        
        self.request.context.OAuthToken = oathToken;
        self.request.context.OAuthTokenSecret = oathSecret;
        [self.request callAPIMethodWithGET:@"flickr.people.getPhotos" arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"me", @"user_id", nil]];
    }
    else{
        [self.request fetchOAuthRequestTokenWithCallbackURL:[NSURL URLWithString:@"login://com.yml.flickr" ]];
    };
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionView Delegates
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return photos.count;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionViewCell" forIndexPath:indexPath];
    NSDictionary* photo = [photos objectAtIndex:indexPath.row];
    NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com", [photo objectForKey:@"farm"]];
    photoURLString = [NSString stringWithFormat:@"%@/%@/%@_%@_s.jpg", photoURLString, [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
    
    NSURL *url = [NSURL URLWithString:photoURLString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    __weak ImageCollectionViewCell* weakCell = cell;
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        ImageCollectionViewCell* weakSelfRef = weakCell;
        if(error)
            NSLog(@"%@",error.localizedDescription);
        else{
            UIImage* image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelfRef.imageView.image = image;
            });
        }
    }];
    return cell;
}
#pragma OFLicker Delegates
- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary
{
    [photos removeAllObjects];
    NSInteger totalPics = [[inResponseDictionary valueForKeyPath:@"photos.total"] integerValue];
    for (int i=0; i<totalPics; i++) {
        NSDictionary *photo = [[inResponseDictionary valueForKeyPath:@"photos.photo"] objectAtIndex:i];
        [photos addObject:photo];
    }
    [self.collectionView reloadData];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError
{
    NSLog(@"%@",[inError localizedDescription]);
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"MyFlickr" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest imageUploadSentBytes:(NSUInteger)inSentBytes totalBytes:(NSUInteger)inTotalBytes
{

}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthRequestToken:(NSString *)inRequestToken secret:(NSString *)inSecret
{
    NSLog(@"%@ , %@",inRequestToken,inSecret);
    inRequest.context.OAuthToken = inRequestToken;
    inRequest.context.OAuthTokenSecret = inSecret;
    NSURL* url = [inRequest.context userAuthorizationURLWithRequestToken:inRequestToken requestedPermission:@""];
    [[UIApplication sharedApplication] openURL:url];

}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthAccessToken:(NSString *)inAccessToken secret:(NSString *)inSecret userFullName:(NSString *)inFullName userName:(NSString *)inUserName userNSID:(NSString *)inNSID
{
    inRequest.context.OAuthToken = inAccessToken;
    inRequest.context.OAuthTokenSecret = inSecret;
    
    [[SharedSuite sharedInstance] setOathToken:inAccessToken];
    [[SharedSuite sharedInstance] setOathSecret:inSecret];
    [[SharedSuite sharedInstance] setLoggedIn:YES];
    [[SharedSuite sharedInstance] save];
    [self.refreshButton setEnabled:YES];
    NSLog(@"User Full Name %@ , username %@",inFullName,inUserName);
    
    [inRequest callAPIMethodWithGET:@"flickr.people.getPhotos" arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"me", @"user_id", nil]];
}

#pragma mark Notifications
- (void) handleURL:(NSNotification*) notif
{
    NSDictionary* userInfo = [notif userInfo];
    NSURL* url = userInfo[@"url"];
    NSURL* baseURL = [NSURL URLWithString:@"login://com.yml.flickr"];
    NSString* requestToken;
    NSString* verifier;
    OFExtractOAuthCallback(url, baseURL, &requestToken, &verifier);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleURL:) name:handleURL object:nil];
    [self.request fetchOAuthAccessTokenWithRequestToken:requestToken verifier:verifier];
    
}

#pragma mark Actions
- (IBAction)onRefresh:(id)sender {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleURL:) name:handleURL object:nil];
    BOOL loggedIn = [[SharedSuite sharedInstance] loggedIn];
    if(loggedIn){
        [self.request callAPIMethodWithGET:@"flickr.people.getPhotos" arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"me", @"user_id", nil]];
    }
}

@end
