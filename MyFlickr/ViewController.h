//
//  ViewController.h
//  MyFlickr
//
//  Created by Arshad on 10/13/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ObjectiveFlickr.h>

@interface ViewController : UIViewController<OFFlickrAPIRequestDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
@end

