//
//  ImageCollectionViewCell.m
//  MyFlickr
//
//  Created by Arshad on 10/14/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (void) prepareForReuse
{
    self.imageView.image = nil;
}
@end
