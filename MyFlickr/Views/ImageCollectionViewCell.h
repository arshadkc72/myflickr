//
//  ImageCollectionViewCell.h
//  MyFlickr
//
//  Created by Arshad on 10/14/14.
//  Copyright (c) 2014 YML. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
